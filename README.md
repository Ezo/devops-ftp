# devops-ftp
Ce code source est en lien avec cet [article](http://blog.ezoqc.com/gitlab-devops-et-deploiement-en-continu-par-ftp-partie-1/).

Pour exécuter sur votre dépôt, modifier le fichier `git-functions.js` pour faire pointer `simple-git` sur votre copie locale du dépôt de votre projet.

Puis :

`node deploy.js`