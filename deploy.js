require('dotenv').config();

const GitFunctions = require('./src/lib/git-functions');
const Git2FtpFunfctions = require('./src/lib/git-2-ftp-functions');
const FtpFunctions = require('./src/lib/ftp-functions');
const DeploymentFunctions = require('./src/lib/deployment-functions');

const FtpClient = process.env.DEBUG === 'true' ? 
    require('./test/lib/ftp-client.mock') : 
    require('ssh2-sftp-client');

const git = new GitFunctions();
const git2ftp = new Git2FtpFunfctions();
const ftp = new FtpFunctions(new FtpClient());
const deploy = new DeploymentFunctions(ftp);

const doDeployment = async () => {
    try {
        const lastCommitDeployedHash = await deploy.getLastCommitHashDeployed();
        const commitsToDeploy = await git.fetchCommitsSinceHash(lastCommitDeployedHash);

        if (commitsToDeploy.length > 0) {
            const ftpActions = git2ftp.convert(commitsToDeploy);
            ftp.executeCommands(ftpActions);
            deploy.updateLastCommitDeployed(commitsToDeploy[0].hash)
        }
    } catch (ex) {
        console.error(ex);
    }
};

doDeployment();