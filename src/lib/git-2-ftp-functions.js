module.exports = class Git2FtpFunctions {
    convert(gitActions) {
        const mergedGitActions = gitActions.flatMap(x => x.files);
        const ftpActions = [];
        for (let i = mergedGitActions.length - 1; i >= 0; i--) {
            const gitAction = mergedGitActions[i];
            switch (gitAction.status) {
                case 'add':
                    addCopyNewIfNotInList(gitAction.path, ftpActions);
                    break;
                case 'edit':
                    addCopyIfNotInList(gitAction.path, ftpActions);
                    break;
                case 'move':
                    addCopyNewIfNotInList(gitAction.path, ftpActions);
                    addDeleteIfNotInList(gitAction.previousPath, ftpActions);
                    break;
                case 'delete':
                    addDeleteIfNotInList(gitAction.path, ftpActions);
                    break;
            }
        }
        return ftpActions;
    }
}

const addCopyNewIfNotInList = (filePath, ftpActions) => {
    let existingFtpAction = ftpActions.find(ftpAction => ftpAction.path === filePath);

    if (existingFtpAction && existingFtpAction.status === 'delete') {
        existingFtpAction.status = 'copy';
    } else if (existingFtpAction && existingFtpAction.status !== 'delete') {
        existingFtpAction.status = 'copy-new';
    } else {
        ftpActions.push({ status: 'copy-new', path: filePath });
    }
}

const addCopyIfNotInList = (filePath, ftpActions) => {
    let existingFtpAction = ftpActions.find(ftpAction => ftpAction.path === filePath);

    if (existingFtpAction && existingFtpAction.status !== 'copy-new') {
        existingFtpAction.status = 'copy';
    } else if (!existingFtpAction) {
        ftpActions.push({ status: 'copy', path: filePath });
    }
}

const addDeleteIfNotInList = (filePath, ftpActions) => {
    const existingFtpAction = ftpActions.find(ftpAction => ftpAction.path === filePath);
    if (existingFtpAction && existingFtpAction.status !== 'copy-new') {
        existingFtpAction.status = 'delete';
    } else if (existingFtpAction && existingFtpAction.status === 'copy-new') {
        const idx = ftpActions.findIndex(ftpAction => ftpAction.path === filePath);
        ftpActions.splice(idx, 1);
    } else {
        ftpActions.push({ status: 'delete', path: filePath });
    }
}