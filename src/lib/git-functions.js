const simpleGit = require('simple-git')(process.env.PATH_TO_LOCAL_GIT_REPO);
//                                      ^^^--- Path to your local repo

module.exports = class GitFunctions {
    fetchCommitsSinceHash(lastCommitHash) {
        return new Promise(async (resolve, reject) => {
            try {
                const hashesToDeploy = await fetchCommitHashesSinceHash(lastCommitHash);

                const commitsToReturn = [];
                for (let i = 0; i < hashesToDeploy.length; i++) {
                    const hash = hashesToDeploy[i];
                    const files = await fetchCommitFilesModifiedFromHash(hash);
                    commitsToReturn.push({hash, files});
                }

                resolve(commitsToReturn);
            } catch (err) {
                reject(err);
            }
        });
    }
}

const fetchCommitHashesSinceHash = lastCommitHash => {
    return new Promise((resolve, reject) => {
        try {
            simpleGit.log((err, log) => {
                if (err) {
                    reject(err);
                    return;
                }
    
                log.all.forEach(x => x.date = new Date(x.date));
                const lastCommit = log.all.find(x => x.hash === lastCommitHash);
                const commitHashesToKeep = log.all
                    .filter(x => x.date.getTime() > lastCommit.date.getTime())
                    .map(x => x.hash);
    
    
                resolve(commitHashesToKeep);
            });
        } catch (err) {
            reject(err);
        }
    });
}

const fetchCommitFilesModifiedFromHash = hash => {
    return new Promise((resolve, reject) => {
        try {
            simpleGit.show([hash], (err, show) => {
                if (err) {
                    reject(err);
                    return;
                }

                const splitShow = show.split('\n');
                const diffs = splitShow.filter(x => x.startsWith('diff --git'));
                const files = [];
                diffs.forEach(currentDiff => {
                    const diffFile = extractFileModificationInfo(currentDiff, splitShow);
                    files.push(diffFile);
                })

                resolve(files);
            });
        } catch (err) {
            reject(err);
        }
    })
};

const extractFileModificationInfo = (diff, splitShow) => {
    const idxOfDiff = splitShow.findIndex(x => x === diff);
    const fileStatusLine = splitShow[idxOfDiff + 1];
    const renameStatusLine = splitShow[idxOfDiff + 2];
    const file = {};
    if (fileStatusLine.startsWith('new file mode')) {
        file.status = 'add';
    } else if (fileStatusLine.startsWith('deleted file mode')) {
        file.status = 'delete';
    } else {
        file.status = 'edit';
    }

    const filesAsString = diff.replace('diff --git ', '').trim();
    if (renameStatusLine.startsWith('rename from')) {
        file.status = 'move';
        file.path = filesAsString.split(' ')[1].replace(/^b\//, '/');
        file.previousPath = filesAsString.split(' ')[0].replace(/^a\//, '/'); 
    } else {
        file.path = filesAsString.split(' ')[0].replace(/^a\//, '/');
    }

    return file;
};
