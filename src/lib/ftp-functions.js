const FtpClient = require('ssh2-sftp-client');

const ftpInfo = {
    "host": "sftp.example.com",
    "port": "22",
    "username": "usr",
    "password": "mdp"
};

module.exports = class FtpFunctions {
    constructor(ftpClient) {
        this.ftp = ftpClient;
    }

    copy(localFile, remoteFile) {
        return new Promise(async (resolve, reject) => {
            try {
                await this.ftp.connect(ftpInfo);
                await this.ftp.put(localFile, remoteFile);
                await this.ftp.end();
                resolve();
            } catch (ex) {
                reject(ex);
            }
        });
    }

    executeCommands(ftpActions) {
        return new Promise(async (resolve, reject) => {
            try {
                await this.ftp.connect(ftpInfo);
                for (let i = 0; i < ftpActions.length; i++) {
                    const ftpAction = ftpActions[i];

                    const localPath = '/home/test/project' + ftpAction.path;
                    const remotePath = '/srv/myapp/project' + ftpAction.path;

                    switch (ftpAction.status) {
                        case 'copy':
                        case 'copy-new':
                            await this.ftp.put(localPath, remotePath);
                            break;
                        case 'delete':
                            await this.ftp.delete(remotePath);
                            break;
                    }
                }

                await this.ftp.end();
                resolve();
            } catch (ex) {
                reject(ex);
            }
        })
    }
}