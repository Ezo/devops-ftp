const request = require('request-promise');
const fs = require('fs');

module.exports = class DeploymentFunctions {
    constructor(ftpFunctions) {
        this.ftp = ftpFunctions;
    }

    getLastCommitHashDeployed() {
        return new Promise(async (resolve, reject) => {
            try {
                const deployInfo = JSON.parse(await request(process.env.DEPLOY_JSON_URL));
                resolve(deployInfo.lastCommitDeployed);
            } catch (ex) {
                reject(ex);
            }
        });
    }

    updateLastCommitDeployed(newHash) {
        return new Promise(async (resolve, reject) => {
            try {
                const deploy = {lastCommitDeployed: newHash};
                const localPathToDeployJson = './deploy.json';
                const remotePathToDeployJson = `${process.env.REMOTE_APP_ROOT}/deploy.json`;
                fs.writeFileSync(localPathToDeployJson, JSON.stringify(deploy));
                await this.ftp.copy(localPathToDeployJson, remotePathToDeployJson)
                fs.unlinkSync(localPathToDeployJson);

                resolve();
            } catch (ex) {
                reject(ex);
            }
        });
    }
}