module.exports = class FtpClient {
    connect(ftpInfo) {
        console.log(`FTP session connected to ${ftpInfo.host}`)
        return Promise.resolve();
    }

    put(local, remote) {
        console.log(`FTP copy ${local} ${remote}`);
        return Promise.resolve();
    }

    delete(remote) {
        console.log(`FTP delete ${remote}`);
        return Promise.resolve();
    }

    end() {
        console.log(`FTP connection closed.`)
        return Promise.resolve();
    }
}