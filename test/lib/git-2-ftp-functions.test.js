const Git2FtpFunctions = require('../../src/lib/git-2-ftp-functions');
const git2ftp = new Git2FtpFunctions();

const dummyFilePath1 = '/file1';
const dummyFilePath2 = '/file2';
const commitAddGitAction = [{ status: 'add', path: dummyFilePath1 }];
const commitEditGitAction = [{ status: 'edit', path: dummyFilePath1 }];
const commitDeleteGitAction = [{ status: 'delete', path: dummyFilePath1 }];
const commitMoveGitAction = [{ status: 'move', path: dummyFilePath1, previousPath: dummyFilePath2 }];

test('Test that an empty array returns an empty array', () => {
    const gitActions = [];
    const ftpActions = git2ftp.convert(gitActions);
    expect(ftpActions.length).toBe(0);
});

test('Test that an "add" git action is converted to a "copy-new" ftp action', () => {
    const gitActions = [commitAddGitAction];
    const ftpActions = git2ftp.convert(gitActions);

    expect(ftpActions.length).toBe(1);
    expect(ftpActions[0].status).toBe('copy-new');
});

test('Test that an "edit" git action is converted to a "copy" ftp action', () => {
    const gitActions = [commitEditGitAction];
    const ftpActions = git2ftp.convert(gitActions);

    expect(ftpActions.length).toBe(1);
    expect(ftpActions[0].status).toBe('copy');
});

test('Test that a "delete" git action is converted to a "delete" ftp action', () => {
    const gitActions = [commitDeleteGitAction];
    const ftpActions = git2ftp.convert(gitActions);

    expect(ftpActions.length).toBe(1);
    expect(ftpActions[0].status).toBe('delete');
});

test('Test that a "move" git action is converted to a "copy-new" and a "delete" ftp action', () => {
    const gitActions = [commitMoveGitAction];
    const ftpActions = git2ftp.convert(gitActions);

    expect(ftpActions.length).toBe(2);
    expect(ftpActions[0].status).toBe('copy-new');
    expect(ftpActions[0].path).toBe(commitMoveGitAction[0].path);
    expect(ftpActions[1].status).toBe('delete');
    expect(ftpActions[1].path).toBe(commitMoveGitAction[0].previousPath);
});

test('Test that an "add" followed by an "edit" git action is converted to a single "copy-new" ftp action', () => {
    const gitActions = [commitEditGitAction, commitAddGitAction];
    const ftpActions = git2ftp.convert(gitActions);

    expect(ftpActions.length).toBe(1);
    expect(ftpActions[0].status).toBe('copy-new');
});

test('Test that an "add" followed by a "delete" git action is converted to a an empty array', () => {
    const gitActions = [commitDeleteGitAction, commitAddGitAction];
    const ftpActions = git2ftp.convert(gitActions);

    expect(ftpActions.length).toBe(0);
});

test('Test that a "delete" followed by an "add" git action is converted to single "copy" ftp action', () => {
    const gitActions = [commitAddGitAction, commitDeleteGitAction];
    const ftpActions = git2ftp.convert(gitActions);

    expect(ftpActions.length).toBe(1);
    expect(ftpActions[0].status).toBe('copy');
});


test('Test that a "move" followed by a "delete" git action is converted to a single "delete" ftp action', () => {
    const gitActions = [commitDeleteGitAction, commitMoveGitAction];
    const ftpActions = git2ftp.convert(gitActions);

    expect(ftpActions.length).toBe(1);
    expect(ftpActions[0].status).toBe('delete');
    expect(ftpActions[0].path).toBe(commitMoveGitAction[0].previousPath);
});